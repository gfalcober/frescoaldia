# -*- encoding: utf-8 -*-

from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import User


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Confirmar contraseña'), widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',)

    def password_match(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("Las contraseñas no coinciden"))
        return password2

    def save(self, commit=True):
        # Save the provided password hashed:
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()

        return user


class UserChangeForm(forms.ModelForm):
    class Meta:
        exclude = ()
        model = User

    def clean_password(self):
        ''' Regardless of what the user provides, return the initial value.
            This is done here, rather than on the field, because the field does
            not have access to the initial value
        '''
        return self.initial['password']
    

class UserAdmin(UserAdmin):
    # Forms to change and add members:
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('email', 'first_name', 'last_name', 'is_active', 'is_staff', 'is_superuser',)
    list_filter = ('is_active',)
    fieldsets = (
        (None, {'fields': ('email',)}),
        (_('Datos Personales'), {'fields': ('first_name', 'last_name',)}),
        (_('Permisos'), {'fields': ('is_staff', 'is_superuser', 'user_permissions', 'groups',)}),
        (_('Estado'), {'fields': ('is_active', 'ever_logged_in', 'date_joined', 'last_login',)}),
    )
    readonly_fields = ('date_joined',)

    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'password1', 'password2',)
        }),
    )

    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()



admin.site.register(User, UserAdmin)
#admin.site.unregister(Group)
