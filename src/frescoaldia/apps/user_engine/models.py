# -*- coding: utf-8 -*-

from django.db import models
from django.dispatch.dispatcher import receiver
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, Group, Permission,
    _user_has_module_perms, _user_has_perm
)

from frescoaldia.apps.main.signals import first_login


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        if not email:
            raise ValueError(_('Email requerido'))

        user = self.model(
            email=UserManager.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        
        return user

    def create_superuser(self, email, first_name, last_name, password):
        user = self.create_user(
            email,
            first_name=first_name,
            last_name=last_name,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class PermissionsMixin(models.Model):
    groups = models.ManyToManyField(Group, verbose_name=_('groups'),
        blank=True, help_text=_('The groups this user belongs to. A user will '
                                'get all permissions granted to each of '
                                'his/her group.'),
        related_name="user_set", related_query_name="user")
    user_permissions = models.ManyToManyField(Permission,
        verbose_name=_('permisos del usuario'), blank=True,
        help_text=_('Permisos específicos del usuario.'),
        related_name="user_set", related_query_name="user")

    
    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through his/her
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                permissions.update(backend.get_group_permissions(self, obj))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        # Otherwise we need to check the backends.
        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)

    class Meta:
        abstract = True


class User(AbstractBaseUser, PermissionsMixin):
    date_joined = models.DateTimeField(auto_now_add=True, 
        verbose_name=_('Fecha de alta'))
    # To check if user has ever logged in (required as last_login and date_joined 
    # are initialized with the same time/date):
    ever_logged_in = models.BooleanField(default=False, 
        verbose_name=_('Ha iniciado sesión'))
    username = models.CharField(max_length=100, blank=True, 
        verbose_name=_('Nombre de usuario'))
    email = models.EmailField(max_length=255, unique=True, db_index=True, 
        verbose_name=_('E-mail'))
    first_name = models.CharField(max_length=50, blank=True, 
        verbose_name=_('Nombre'))
    last_name = models.CharField(max_length=80, blank=True, 
        verbose_name=_('Apellidos'))
    is_active = models.BooleanField(default=True, db_index=True, 
        verbose_name=_('Activo'))
    is_staff = models.BooleanField(default=False, db_index=True, 
        verbose_name=_('Staff'))
    is_superuser = models.BooleanField(default=False, 
        verbose_name=_('Administrador'))

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        db_table = 'Users'
        verbose_name = 'usuario'


    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email

    '''def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True'''


# ------------------------------------------------------------------------------
# Signal handlers
# ------------------------------------------------------------------------------

# Updates 'ever_logged_in' User field after first login:
@receiver(first_login, sender=User)
def user_first_login_handler(sender, **kwargs):
    kwargs['instance'].ever_logged_in = True
    kwargs['instance'].save(update_fields=('ever_logged_in',))
