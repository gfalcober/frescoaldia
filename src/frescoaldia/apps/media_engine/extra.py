# -*- coding: utf-8 -*-

from PIL import Image
from subprocess import check_output, CalledProcessError

from django.conf import settings
from django.db.models.signals import post_delete

from .signals import video_ready


def get_file_path(instance, is_thumbnail):
    # Returns save path according to object type (Image, Slide, Video... or thumbnail)
    path = settings.MEDIA_ROOT + instance._meta.db_table.lower() + '/'
    #if instance.product: path += 'products/'
    if is_thumbnail: path += 'thumbnails/'
    filename = instance.media_file.name.rpartition('/')[2].rpartition('.')[0]
    extension = settings.MEDIA_FORMATS['Thumbnail'] \
        if is_thumbnail else settings.MEDIA_FORMATS[instance.__class__.__name__]

    return instance.media_file.storage.get_available_name(path+filename+extension)


def get_img_thumbnail(instance):
    # Generate thumbnail from image file and return filepath to model save()
    img = Image.open(instance.media_file)
    size = (settings.IMAGE_THUMBNAIL_SIZE, settings.IMAGE_THUMBNAIL_SIZE)
    
    img.thumbnail(size, Image.ANTIALIAS)
    fullpath = get_file_path(instance, True)
    img.save(fullpath, 'JPEG')

    return fullpath.rpartition('media/')[2]


def get_vid_thumbnail(instance, video_path):
    # Generate thumbnail from video and return filepath to model save()
        fullpath = get_file_path(instance, True)
        try:
            check_output(("ffmpeg", "-i", video_path, "-ss", "00:00:02", \
                "-f", "image2", "-vframes", "1", "-vf", "scale="+\
                str(settings.VIDEO_THUMBNAIL_SIZE)+":-1", fullpath,))
            fullpath = fullpath.rpartition('media/')[2]
        except CalledProcessError:
            instance.media_file.storage.delete(instance.media_file.path)
            fullpath = ''

        return fullpath


def process_image(instance):
    # Transcode image into JPEG and return filepath to model save()
    img = Image.open(instance.media_file)
    if instance.media_file.width > settings.IMAGE_MAX_WIDTH or \
        instance.media_file.height > settings.IMAGE_MAX_HEIGHT:
        size = (settings.IMAGE_MAX_WIDTH, settings.IMAGE_MAX_HEIGHT)
        img.thumbnail(size, Image.ANTIALIAS)

    fullpath = get_file_path(instance, False)
    img.save(fullpath, 'JPEG')

    return fullpath.rpartition('media/')[2]


def process_video(instance):
    # Transcode video into WebM and send signal when finished
    filepath = get_file_path(instance, False)
    try:
        check_output(("ffmpeg", "-i", instance.media_file.path, \
            "-acodec", "libvorbis", "-vcodec", "libvpx", filepath,))
    except CalledProcessError:
        # Remove files:
        post_delete.send(sender=instance.__class__, instance=instance)
        instance.delete()
    return filepath
