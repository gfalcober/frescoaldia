# -*- coding: utf-8 -*-

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch.dispatcher import receiver
from django.utils.translation import ugettext_lazy as _
# Django 1.7:
#from django.contrib.contenttypes.fields import GenericRelation
# Django 1.6:
#from django.contrib.contenttypes.generic import GenericRelation
from django.conf import settings

from .extra import (
    process_image, process_video, get_img_thumbnail, get_vid_thumbnail
)
from .signals import video_ready


def get_path(instance, filename): 
    path = instance._meta.db_table.lower() + '/'
    #if instance.product: path += 'products/'
    return path


# ------------------------------------------------------------------------------
# Models:
# ------------------------------------------------------------------------------

class BaseObject(models.Model):
    # Django 1.7:
    #product = GenericRelation(BaseProduct, editable=False, 
    #    related_query_name='product_%(class)s', verbose_name=_('Producto'))
    # Django 1.6:
    #product = GenericRelation('main.BaseProduct', editable=False, 
    #    verbose_name=_('Producto'))
    active = models.BooleanField(db_index=True, default=True, 
        verbose_name=_('Activo'))
    thumbnail = models.ImageField(upload_to=get_path, max_length=255, 
        editable=False)
    
    def save(self, *args, **kwargs):
        if self.pk is not None:
            # Object is not new, check if image is being updated:
            previous_file = self.__class__.objects.get(pk=self.pk)
            storage, path = self.media_file.storage, self.media_file.path
            if not storage.exists(path) or \
                storage.size(path) != previous_file.media_file.storage.size(previous_file.media_file.path):
                # Seems like modifying image, so remove previous one and update:
                # Send post_delete signal to remove previous files:
                post_delete.send(sender=self.__class__, instance=previous_file)
                self.media_file = process_image(self)
                self.thumbnail = get_img_thumbnail(self)
        else:
            self.media_file = process_image(self)
            self.thumbnail = get_img_thumbnail(self)
        super(BaseObject, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.media_file.name

    class Meta:
        abstract = True


class Image(BaseObject):
    media_file = models.ImageField(upload_to=get_path, max_length=255, 
        verbose_name=_('Imagen'))
    alt_text = models.CharField(max_length=400, 
        verbose_name=_('Texto alternativo (campo "alt")'))

    class Meta:
        db_table = 'Images'
        verbose_name = _('Imagen')
        verbose_name_plural = _('Imágenes')


class Slide(BaseObject):
    media_file = models.ImageField(upload_to=get_path, max_length=255, 
        verbose_name=_('Imagen'))
    text = models.CharField(max_length=500, verbose_name=_('Texto del slide'))
    text_color = models.CharField(max_length=20, 
        choices=settings.SLIDE_TEXT_COLORS, default='Negro', 
        verbose_name=_('Color del texto'))
    text_h_position = models.CharField(max_length=20, 
        choices=settings.SLIDE_TEXT_H_POSITION, default='Izquierda', 
        verbose_name=_('Alineación del texto'))
    text_v_position = models.CharField(max_length=20, 
        choices=settings.SLIDE_TEXT_V_POSITION, default='Arriba', 
        verbose_name=_('Posición del texto'))

    class Meta:
        db_table = 'Slides'
        verbose_name = 'Slide'


class Video(BaseObject):
    media_file = models.FileField(upload_to='videos/', max_length=255, 
        verbose_name=_('Vídeo'))
    # ready=True after video file is transcoded into WebM:
    ready = models.BooleanField(default=False, editable=False, 
        verbose_name=_('Preparado'))

    def remove_video(self):
        try:
            previous_file = Video.objects.get(pk=self.pk)
            post_delete.send(sender=Video, instance=previous_file)
        except ObjectDoesNotExist:
            pass

    def video_is_new(self):
        ''' File is considered new if filename (without extension) is different 
            than the current file's. Since we use transcoding into WebM, 
            it's not worth comparing filesizes between current and new files 
            as they would probably not match ever.
        '''
        # TODO: Improve comparison criterion (not just filename without extension)
        
        current = Video.objects.get(pk=self.pk).media_file.name\
            .rpartition('/')[2].rpartition('.')[0]
        new = self.media_file.name.rpartition('/')[2].rpartition('.')[0]
        
        return new != current

    
    def save(self, *args, **kwargs):
        self.new_video = False
        if self.pk is not None:
            if 'update_fields' not in kwargs and self.video_is_new():
                self.remove_video()
                self.new_video = True
        else:
            self.new_video = True

        super(BaseObject, self).save(*args, **kwargs)


    class Meta:
        db_table = 'Videos'
        verbose_name = _('vídeo')


# ------------------------------------------------------------------------------
# Signal handlers
# ------------------------------------------------------------------------------

@receiver(post_delete)
def media_post_delete_handler(sender, **kwargs):
    ''' Remove multimedia files after the corresponding db entries are deleted 
        or prior to updating file field:
    '''
    if sender.__name__ in ('Image', 'Slide', 'Video',):
        kwargs['instance'].media_file.storage.delete(kwargs['instance'].media_file.path)
        # Thumbnail:
        try:
            kwargs['instance'].thumbnail.storage.delete(kwargs['instance'].thumbnail.path)
        except ValueError:
            # No thumbnail:
            pass


@receiver(post_save, sender=Video)
def video_post_save_handler(sender, **kwargs):
    if kwargs['instance'].new_video:
        video_ready.send(
            sender=Video,
            instance=kwargs['instance'],
            filepath=process_video(kwargs['instance'])
        )


@receiver(video_ready, sender=Video)
def video_ready_handler(sender, **kwargs):
    # Remove original video:
    kwargs['instance'].media_file.storage.delete(kwargs['instance'].media_file.path)
    # Update object in db:
    kwargs['instance'].ready = True
    kwargs['instance'].media_file.name = kwargs['filepath'].rpartition('media/')[2]
    # Get thumbnail from video:
    kwargs['instance'].thumbnail = get_vid_thumbnail(kwargs['instance'], kwargs['filepath'])
    # Save:
    kwargs['instance'].save(update_fields=('thumbnail', 'media_file', 'ready',))
