# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin

from .models import Image, Slide, Video


class BaseAdmin(admin.ModelAdmin):
    list_display = ('media_file', 'active',)
    list_filter = ('active',)
    search_fields = ()


class ImageChangeForm(forms.ModelForm):
    def save(self, commit=True):
        super(self.__class__, self).save(commit=True, update_fields=self.changed_data)
    
    class Meta:
        model = Image
        exclude = ('thumbnail',)


class SlideAdmin(BaseAdmin):
    list_display = ('media_file', 'active', 'text',)


class SlideChangeForm(ImageChangeForm):
    class Meta:
        model = Slide


class VideoChangeForm(ImageChangeForm):
    class Meta:
        model = Video


admin.site.register(Image, BaseAdmin)
admin.site.register(Slide, SlideAdmin)
admin.site.register(Video, BaseAdmin)
