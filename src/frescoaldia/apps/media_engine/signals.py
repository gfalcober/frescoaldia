# -*- coding: utf-8 -*-

import django.dispatch


# Sent after new video is transcoded:
video_ready = django.dispatch.Signal(providing_args=['sender', 'instance', 'filepath'])
