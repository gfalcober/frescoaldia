# -*- coding: utf-8 -*-

from django.views.generic.base import TemplateView, View
from django.core.urlresolvers import resolve

from frescoaldia.apps.user_engine.models import User
#from .models import 
from .signals import first_login


# Mixins -----------------------------------------------------------------------

class NewUser(object):
    # If the user logs in for the first time, sends signal and add variable to
    # context for further actions:
    def get_context_data(self, **kwargs):
        if self.request.user.is_authenticated() and not self.request.user.ever_logged_in:
            # Send signal to notify user's first login:
            first_login.send(sender=User, instance=self.request.user)
            kwargs['first_login'] = True
        else:
            kwargs['first_login'] = False


# Views ------------------------------------------------------------------------

class Index(NewUser, TemplateView):
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        
        return context


class Sections(NewUser, TemplateView):
    template_name = 'main/section.html'

    def get_context_data(self, **kwargs):
        context = super(Sections, self).get_context_data(**kwargs)
        
        return context


class Search(NewUser, View):
    template_name = 'main/search.html'

    def get(self, *args, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        return render(self.request, self.template_name, context)

    def post(self, *args, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        return render(self.request, self.template_name, context)


class GenericView(NewUser, TemplateView):
    def get_template_names(self):
        return  [
                   'main/flatpages/'
                   + resolve(self.request.path_info).url_name
                   + '.html'
                ]

    def get_context_data(self, **kwargs):
        context = super(GenericView, self).get_context_data(**kwargs)
        return context
