# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Brand, Milk, Yoghurt, Bread, Egg


class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'contact', 'contact_email', 'telephone', 
        'comments',)
    search_fields = ('name', 'contact', 'contact_email',)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('brand', 'name', 'price', 'unit_quantity', 'total_quantity', 
        'stock', 'sales', 'rate', 'on_sale',)
    list_filter = ('brand', 'on_sale',)
    search_fields = ('brand', 'name', 'description',)


admin.site.register(Brand, BrandAdmin)
admin.site.register(Milk, ProductAdmin)
admin.site.register(Yoghurt, ProductAdmin)
admin.site.register(Bread, ProductAdmin)
admin.site.register(Egg, ProductAdmin)
