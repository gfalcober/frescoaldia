# -*- coding: utf-8 -*-

import django.dispatch

# Sent after user's first login:
first_login = django.dispatch.Signal(providing_args=['sender', 'instance'])
