# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify


class Base(models.Model):
    name = models.CharField(max_length=150, verbose_name=_('Nombre'))
    name_id = models.CharField(max_length=150, db_index=True, editable=False,
        verbose_name=_('ID'))

    def save(self, *args, **kwargs):
        self.name_id = slugify(self.name)
        super(Base, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class Brand(Base):
    contact = models.CharField(max_length=150, db_index=True, 
        verbose_name=_('Persona de contacto'))
    contact_email = models.EmailField(max_length=254, unique=True, 
        verbose_name=_('Email'))
    telephone = models.PositiveIntegerField(blank=True, null=True, unique=True,
        verbose_name=_('Teléfono'))
    comments = models.CharField(max_length=200, blank=True, null=True, 
        verbose_name=_('Comentarios'))
    total_billed = models.DecimalField(max_digits=14, decimal_places=2, 
        db_index=True, verbose_name=_('Facturado'))
    earnings = models.DecimalField(max_digits=14, decimal_places=2, 
        db_index=True, verbose_name=_('Beneficios'))
    logo = models.ForeignKey('media_engine.Image', verbose_name=_('Logo'))

    class Meta:
        db_table = 'Brands'
        verbose_name = _('Fabricantes')
        verbose_name_plural = _('Fabricantes')


class BaseProduct(Base):
    CATEGORIES = (
        (_('Dairy'), (
            ('Milk', _('Leche')),
            ('Yoghurts', _('Yogures')),
            ('Cheese', _('Quesos')),
            ('Butter', _('Mantequilla')),
        )),
        (_('Eggs'), (
            ('Hen', _('Gallina')),
            ('Ostrich', _('Avestruz')),
            ('Quail', _('Codorniz')),
        )),
        (_('Bread'), (
            ('Wheat', _('Trigo')),
            ('Rye', _('Centeno')),
        )),
    )

    brand = models.ForeignKey(Brand, editable=False, 
        verbose_name=_('Fabricante'))
    category = models.CharField(max_length=100, blank=True, null=True, 
        default=None, choices=CATEGORIES, db_index=True, 
        verbose_name=_('Categoría'))
    description = models.TextField(blank=True, null=True, default=None, 
        verbose_name=_('Descripción'))
    related_info = models.URLField(max_length=255, blank=True, null=True, 
        verbose_name=_('Enlace +info'))
    origin = models.CharField(max_length=120, db_index=True, 
        verbose_name=_('Procedencia'))
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=True, 
        null=True, default=None, verbose_name=_('Precio'))
    unit_quantity = models.CharField(db_index=True, max_length=50, blank=True, 
        null=True, default=None, verbose_name=_('Cantidad unitaria'))
    total_quantity = models.SmallIntegerField(blank=True, null=True, 
        default=None, verbose_name=_('Cantidad total'))
    on_sale = models.BooleanField(db_index=True, default=True, 
        verbose_name=_('A la venta'))
    stock = models.PositiveIntegerField(db_index=True, default=0, 
        verbose_name=_('Unidades en stock'))
    on_sale_since = models.DateField(db_index=True, auto_now_add=True, 
        editable=False, verbose_name=_('A la venta desde'))
    last_sale = models.DateTimeField(db_index=True, editable=False, blank=True, 
        null=True, default=None, verbose_name=_('Última venta'))
    sales = models.PositiveIntegerField(db_index=True, default=0, 
        verbose_name=_('Ventas totales'))
    refunds = models.PositiveIntegerField(db_index=True, default=0, 
        verbose_name=_('Devoluciones'))
    rate = models.DecimalField(db_index=True, max_digits=3, decimal_places=1, 
        blank=True, null=True, default=None, verbose_name=_('Valoración'))
    votes = models.PositiveIntegerField(blank=True, null=True, default=0,
        verbose_name=_('Nº votos'))

    class Meta:
        abstract = True


class Milk(BaseProduct):
    SUBCATEGORIES = (
        ('Whole', _('Entera')),
        ('Semi-skimmed', _('Semidesnatada')),
        ('Skimmed', _('Desnatada')),
    )

    subcategory = models.CharField(db_index=True, max_length=100, blank=True, 
        null=True, default=None, choices=SUBCATEGORIES, 
        verbose_name=_('Subcategoría'))
    lactose_free = models.BooleanField(db_index=True, default=False, 
        verbose_name=_('Sin lactosa'))

    class Meta:
        db_table = 'Milk'
        verbose_name = _('Leche')
        verbose_name_plural = _('Leche')
    

class Yoghurt(BaseProduct):
    SUBCATEGORIES = (
        ('Plain', _('Natural')),
        ('Fruits', _('De frutas')),
        ('Sweetened', _('Azucarado')),
        ('Edulcorated', _('Edulcorado')),
    )
    subcategory = models.CharField(db_index=True, max_length=100, blank=True, 
        null=True, default=None, choices=SUBCATEGORIES, 
        verbose_name=_('Subcategoría'))
    lactose_free = models.BooleanField(db_index=True, default=False, 
        verbose_name=_('Sin lactosa'))

    class Meta:
        db_table = 'Yoghurts'
        verbose_name = _('Yogur')
        verbose_name_plural = _('Yogures')


class Bread(BaseProduct):
    SUBCATEGORIES = (
        ('White', _('Pan blanco')),
        ('Whole_wheat', _('Pan integral')),
        ('Multigrain', _('Pan con cereales')),
    )

    subcategory = models.CharField(db_index=True, max_length=100, blank=True, 
        null=True, default=None, choices=SUBCATEGORIES, 
        verbose_name=_('Subcategoría'))
    gluten_free = models.BooleanField(db_index=True, default=False, 
        verbose_name=_('Sin gluten'))

    class Meta:
        db_table = 'Bread'
        verbose_name = _('Pan')
        verbose_name_plural = _('Pan')


class Egg(BaseProduct):
    SIZES = (
        ('S', 'S'),
        ('M', 'M'),
        ('L', 'L'),
        ('XL', 'XL'),
        ('var', 'Varios tamaños'),
    )

    size = models.CharField(db_index=True, max_length=20, blank=True, 
        null=True, default=None, choices=SIZES, verbose_name=_('Tamaño'))

    class Meta:
        db_table = 'Eggs'
        verbose_name = _('Huevo')
