# -*- encoding: utf-8 -*-

import os

from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings

from .apps.main.views import Index, Sections, GenericView


admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # Apps' URLs:
    url(r'^accounts/', include('allauth.urls')),
    # Website URLs:
    url(r'^$', Index.as_view(), name='index'),
    url(r'^sections$', Sections.as_view(), name='sections'),
    url(r'^sections/(?P<section>[\w\-]+)-(?P<id>[\d]+)$', Sections.as_view(), name='section'),
    # Flatpages:
    url(r'^legal$', GenericView.as_view(), name='legal_notice'),
    url(r'^privacy$', GenericView.as_view(), name='privacy'),
    url(r'^about$', GenericView.as_view(), name='about'),
]

if 'OPENSHIFT_DATA_DIR' not in os.environ:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # Not for use in production!
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # Not for use in production!
