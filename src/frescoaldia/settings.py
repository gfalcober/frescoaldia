# -*- coding: utf-8 -*-

import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = 'c8klgwm$_9sn_drfik=4hrlaoczu+a6vyez4e%w68lx&%^&*^9'

DEBUG = True
TEMPLATE_DEBUG = True

SITE_ID = 1

AUTH_USER_MODEL = 'user_engine.User'

ADMINS = (
    ('Gabriel Faet', 'gfalcober@gmail.com'),
)

ALLOWED_HOSTS = ['frescoaldia.com']


INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites', # Required by 'allauth'
    'frescoaldia.apps.main',
    'frescoaldia.apps.user_engine',
    'frescoaldia.apps.media_engine',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    #Auth Providers:
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.twitter',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.static',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)


ROOT_URLCONF = 'frescoaldia.urls'

WSGI_APPLICATION = 'frescoaldia.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'frescoaldia/templates'),
    os.path.join(BASE_DIR, 'frescoaldia/templates/account'),
)


DATABASES = {
    'default': {
        'ENGINE' : 'django.db.backends.postgresql_psycopg2',
        'NAME': 'frescoaldia',
        'USER': 'adminkt9gdaa',
        'PASSWORD': 'JFwTWj5fyKPm',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# Internationalization ---------------------------------------------------------
LANGUAGE_CODE = 'es-ES'

TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files -----------------------------------------------------------------
STATIC_ROOT = os.path.join(BASE_DIR, 'frescoaldia/static/')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'frescoaldia/media/')
MEDIA_URL = '/media/'


# EMAIL settings ---------------------------------------------------------------
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'    

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'trotterworldplus'
EMAIL_HOST_PASSWORD = 'T3d1g0tr1g0.'
DEFAULT_FROM_EMAIL = 'TEST@TEST.com'


# ------------------------------------------------------------------------------
# ALLAUTH settings
# ------------------------------------------------------------------------------
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/'

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 1
ACCOUNT_EMAIL_SUBJECT_PREFIX = 'Frescoaldia.com - '


# ------------------------------------------------------------------------------
# media_engine settings
# ------------------------------------------------------------------------------
IMAGE_THUMBNAIL_SIZE = 250
VIDEO_THUMBNAIL_SIZE = 320
IMAGE_MAX_HEIGHT = 600
IMAGE_MAX_WIDTH = 1000

SLIDE_TEXT_COLORS = (
    (' ', 'Negro'),
    ('txt_white', 'Blanco'),
)

SLIDE_TEXT_H_POSITION = (
    ('slide_txt_left', 'Izquierda'),
    ('slide_txt_right', 'Derecha'),
)

SLIDE_TEXT_V_POSITION = (
    ('slide_txt_top', 'Arriba'),
    ('slide_txt_bottom', 'Abajo'),
)

MEDIA_FORMATS = {
    'Video': '.webm',
    'Image': '.jpg',
    'Slide': '.jpg',
    'Thumbnail': '.jpg'
}


# ------------------------------------------------------------------------------
# OpenShift settings
# ------------------------------------------------------------------------------
if 'OPENSHIFT_DATA_DIR' in os.environ:
    DATABASES['default'] = {
        'ENGINE' : 'django.db.backends.postgresql_psycopg2',
        'NAME': 'frescoaldia',
        'USER': 'adminhqqng6x',
        'PASSWORD': 'ul41YxwdUrWJ',
        'HOST': '127.4.11.130',
        'PORT': '5432',
    }

    STATIC_ROOT = os.path.join(os.environ.get('OPENSHIFT_REPO_DIR'), 'wsgi', 'static')

    MEDIA_ROOT = os.path.join(os.environ.get('OPENSHIFT_DATA_DIR'), 'media/')
    MEDIA_URL = STATIC_URL + 'media/'

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
